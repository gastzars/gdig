Rails.application.routes.draw do
  get 'gasza_admin', to: 'administrator#index'
  get 'gasza_admin/monitor', to: 'administrator#monitor'
  get 'gasza_admin/topup', to: 'administrator#topup'

  get 'terms/term_of_services'

  get 'terms/privacy_policy'

  get 'terms/refund_policy'

  get 'setting/index'

  get 'verify_user/index'
  get 'verify_user', to: 'verify_user#index'

  get 'contact/index'
  get 'contact', to: 'contact#index'

  get 'order_history/index'
  get 'order_history', to: 'order_history#index'
  get 'order_refresh', to: 'order_history#refresh'

  post 'order_update', to: 'order_history#update_order'
  post 'order_cancel', to: 'order_history#cancel_order'
  post 'report_instance', to: 'order_history#report_instance'

  post 'update_user_notification', to: 'home#update_user_notification'
  post 'cancel_user_notification', to: 'home#cancel_user_notification'

  post 'load_notification', to: 'home#load_notification'
  post 'update_notification_seen', to: 'home#update_notification_seen'
  post 'update_notification_read_all', to: 'home#update_notification_read_all'

  get 'transaction_history/index'
  get 'transaction_history', to: 'transaction_history#index'

  get 'setting/index'
  get 'setting', to: 'setting#index'

  get 'notification/:id', to: 'home#redirect_noti'

  get 'topup/index'
  get 'topup', to: 'topup#index'
  post 'topup_true', to: 'topup#create_true'
  get 'topup_true_verify', to: 'topup#verify_true'
  post 'topup_paypal_verify', to: 'topup#verify_paypal'
  post 'topup_debit', to: 'topup#create_debit'

  get 'home/index'
  get 'home', to: 'home#index'
  post 'create_order', to: 'home#create_order'
  post 'cancel_order', to: 'home#cancel_order'

  get 'reg/index'
  get 'reg', to: 'reg#index'
  post 'reg', to: 'reg#create'

  post 'change_password', to: 'reg#change_password'
  post 'user_signin', to: 'reg#user_signin'
  get 'logout', to: 'reg#logout'

  get 'main/index'
  root 'main#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
