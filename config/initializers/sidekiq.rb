Sidekiq.configure_server do |config|
  config.redis = { url: "redis://#{Playdigger::RedisConfig["host"]}:#{Playdigger::RedisConfig["port"]}/#{Playdigger::RedisConfig["db"]}" }
end

Sidekiq.configure_client do |config|
  config.redis = { url: "redis://#{Playdigger::RedisConfig["host"]}:#{Playdigger::RedisConfig["port"]}/#{Playdigger::RedisConfig["db"]}" }
end
