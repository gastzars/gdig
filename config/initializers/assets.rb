# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )

Rails.application.config.assets.paths << Rails.root.join("app", "assets", "fonts")

Rails.application.config.assets.precompile << /\.(?:svg|eot|woff|ttf)$/

Rails.application.config.assets.precompile += %w( main.css )
Rails.application.config.assets.precompile += %w( main.js )
#Rails.application.config.assets.precompile += %w( fortawesome.js )
Rails.application.config.assets.precompile += %w( reg.css )
Rails.application.config.assets.precompile += %w( reg.js )
Rails.application.config.assets.precompile += %w( home.css )
Rails.application.config.assets.precompile += %w( home.js )
Rails.application.config.assets.precompile += %w( contact.css )
Rails.application.config.assets.precompile += %w( contact.js )
Rails.application.config.assets.precompile += %w( topup.css )
Rails.application.config.assets.precompile += %w( topup.js )
Rails.application.config.assets.precompile += %w( transaction_history.css )
Rails.application.config.assets.precompile += %w( transaction_history.js )
Rails.application.config.assets.precompile += %w( order_history.css )
Rails.application.config.assets.precompile += %w( order_history.js )
Rails.application.config.assets.precompile += %w( setting.css )
Rails.application.config.assets.precompile += %w( setting.js )
Rails.application.config.assets.precompile += %w( verify_user.css )
Rails.application.config.assets.precompile += %w( verify_user.js )
Rails.application.config.assets.precompile += %w( administrator.css )
Rails.application.config.assets.precompile += %w( administrator.js )

Rails.application.config.assets.precompile += %w( terms.css )
Rails.application.config.assets.precompile += %w( terms.js )
