# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180308104801) do

  create_table "announcements", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.text "image_url"
    t.text "title"
    t.text "description"
    t.integer "created_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_time"], name: "index_announcements_on_created_time"
  end

  create_table "money_transactions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "user_id"
    t.string "password"
    t.integer "created_time"
    t.string "status"
    t.integer "amount"
    t.integer "price"
    t.string "topup_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["password"], name: "index_money_transactions_on_password"
    t.index ["user_id", "created_time"], name: "index_money_transactions_on_user_id_and_created_time"
    t.index ["user_id", "created_time"], name: "uid_ct_mn"
    t.index ["user_id"], name: "index_money_transactions_on_user_id"
  end

  create_table "notifications", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "user_id"
    t.integer "notification_type"
    t.integer "created_time"
    t.boolean "is_read"
    t.text "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_time"], name: "index_notifications_on_created_time"
    t.index ["user_id", "created_time"], name: "index_notifications_on_user_id_and_created_time"
  end

  create_table "order_transactions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "user_id"
    t.string "url"
    t.integer "credit"
    t.integer "finished_credit"
    t.text "order_key"
    t.string "username"
    t.integer "created_time"
    t.boolean "is_finished", default: false
    t.string "status", default: "A"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_limit", default: false
    t.integer "instance_number"
    t.index ["is_limit"], name: "index_order_transactions_on_is_limit"
    t.index ["order_key"], name: "index_order_transactions_on_order_key", length: { order_key: 255 }
    t.index ["user_id", "is_finished", "created_time"], name: "uid_if_ct_od"
    t.index ["user_id", "is_finished", "created_time"], name: "uiisct"
    t.index ["user_id", "is_finished"], name: "index_order_transactions_on_user_id_and_is_finished"
    t.index ["user_id"], name: "index_order_transactions_on_user_id"
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "username"
    t.string "password"
    t.string "email"
    t.string "confirm_token"
    t.boolean "is_confirmed", default: false
    t.integer "credit"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "last_seen_announcement_time", default: 0
    t.integer "last_seen_notification_time", default: 0
    t.string "auth"
    t.string "p256dh"
    t.text "endpoint_notification"
    t.boolean "can_send_notification", default: false
    t.integer "total_topup", default: 0
    t.index ["confirm_token"], name: "index_users_on_confirm_token"
    t.index ["email"], name: "index_users_on_email"
    t.index ["username"], name: "index_users_on_username"
  end

end
