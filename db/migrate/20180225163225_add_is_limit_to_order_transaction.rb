class AddIsLimitToOrderTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :order_transactions, :is_limit, :boolean, default: false
    add_column :order_transactions, :instance_number, :integer
    add_index :order_transactions, [:user_id, :is_finished, :created_time], name: 'uid_if_ct_od'
    add_index :order_transactions, :is_limit
  end
end
