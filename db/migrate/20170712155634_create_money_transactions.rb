class CreateMoneyTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :money_transactions do |t|
      t.integer :user_id
      t.string :password
      t.integer :created_time
      t.string :status
      t.integer :amount
      t.integer :price
      t.string :topup_type

      t.timestamps
    end
    add_index :money_transactions, :user_id
    add_index :money_transactions, [:user_id, :created_time]
    add_index :money_transactions, :password
  end
end
