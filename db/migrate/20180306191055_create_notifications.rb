class CreateNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :notifications do |t|
      t.integer :user_id
      t.integer :notification_type
      t.integer :created_time
      t.boolean :is_read, default: false
      t.text :data

      t.timestamps
    end
    add_index :notifications, :created_time
    add_index :notifications, [:user_id, :created_time]
  end
end
