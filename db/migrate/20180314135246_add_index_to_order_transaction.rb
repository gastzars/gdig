class AddIndexToOrderTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :order_transactions, :is_free, :boolean, default: false
    add_index :order_transactions, [:url, :username, :is_free], name: 'ul_un_if'
  end
end
