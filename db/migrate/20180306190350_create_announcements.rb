class CreateAnnouncements < ActiveRecord::Migration[5.1]
  def change
    create_table :announcements do |t|
      t.text :image_url
      t.text :title
      t.text :description
      t.integer :created_time

      t.timestamps
    end
    add_index :announcements, :created_time
  end
end
