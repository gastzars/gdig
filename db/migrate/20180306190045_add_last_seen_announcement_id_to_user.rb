class AddLastSeenAnnouncementIdToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :last_seen_announcement_time, :integer, default: 0
    add_column :users, :last_seen_notification_time, :integer, default: 0
  end
end
