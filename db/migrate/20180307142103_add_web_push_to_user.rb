class AddWebPushToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :auth, :string
    add_column :users, :p256dh, :string
    add_column :users, :endpoint_notification, :text
    add_column :users, :can_send_notification, :boolean, default: false
  end
end
