class AddTotalTopupToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :total_topup, :integer, default: 0
  end
end
