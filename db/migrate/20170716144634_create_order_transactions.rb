class CreateOrderTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :order_transactions do |t|
      t.integer :user_id
      t.string :url
      t.integer :credit
      t.integer :finished_credit
      t.text :order_key, limit: 1000
      t.string :username
      t.integer :created_time
      t.boolean :is_finished, default: false
      t.string :status, default: 'A'

      t.timestamps
    end
    add_index :order_transactions, :user_id
    add_index :order_transactions, [:user_id, :is_finished]
    add_index :order_transactions, [:user_id, :is_finished, :created_time], name: 'uiisct'
    add_index :order_transactions, :order_key, :length => { :order_key => 255 }
  end
end
