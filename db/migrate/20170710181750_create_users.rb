class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :username
      t.string :password
      t.string :email
      t.string :confirm_token
      t.boolean :is_confirmed, default: 0
      t.integer :credit

      t.timestamps
    end
    add_index :users, :confirm_token
    add_index :users, :username
    add_index :users, :email
  end
end
