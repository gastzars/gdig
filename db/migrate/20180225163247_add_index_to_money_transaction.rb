class AddIndexToMoneyTransaction < ActiveRecord::Migration[5.1]
  def change
    add_index :money_transactions, [:user_id, :created_time], name: 'uid_ct_mn'
  end
end
