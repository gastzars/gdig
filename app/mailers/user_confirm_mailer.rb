class UserConfirmMailer < ApplicationMailer
  default :from => "contact.playdigger@gmail.com"

  def registration_confirmation(email, uri, name)
    @uri = uri
    @name = name
    mail(:to => email, :subject => "ยินดีต้อนรับ #{name} เข้าสู่ Playdigger.com")
  end
end
