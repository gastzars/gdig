module ApplicationHelper

  def signin(username, password)
    user = User.find_by(username: username)
    if user && authenticate(user, password)
      login(user)
      true
    else
      raise 'Username หรือ Password ผิดพลาด กรุณาลองใหม่'
    end
  end

  def authenticate(user, password)
    if user
      user.password == password
    else
      false
    end
  end

  def login(user)
    session[:user_id] = user.id
  end

  def signout
    session[:user_id] = nil
  end

  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
  end

  def validate_recaptcha
    recaptcha_secret = '6Lf6AyEUAAAAACQVpWY71Ua6DYN60N_8r6Jk6yY4'
    response = RestClient.post(
      'https://www.google.com/recaptcha/api/siteverify',
      {
        secret: recaptcha_secret,
        response: params['g-recaptcha-response'],
        remoteip: request.remote_ip
      }
    )
    parsed_response = JSON.parse response.to_s
    raise 'ระบบตรวจสอบพบว่าคุณอาจจะเป็นระบบอัตโนมัติ กรุณาลองใหม่' if parsed_response['success'] == false
  end
end
