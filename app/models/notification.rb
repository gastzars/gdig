class Notification < ApplicationRecord
  serialize :data
  before_create :update_created_time

  def get_title
    case notification_type
    when 0
      "การเติมเงินเสร็จสมบูรณ์ ได้รับเครดิต #-#1".gsub("#-#1", data[:amount].to_s)
    when 1
      "การเติมเงินล้มเหลว"
    when 2
      "คำสั่งขุดล้มเหลว #-#1 / #-#2".gsub("#-#1", data[:finished_credit].to_s).gsub("#-#2", data[:credit].to_s)
    when 3
      "คำสั่งขุดเสร็จสมบูรณ์ #-#1 / #-#2".gsub("#-#1", data[:finished_credit].to_s).gsub("#-#2", data[:credit].to_s)
    end
  end

  def get_raw_title
    case notification_type
    when 0
      "การเติมเงินเสร็จสมบูรณ์"
    when 1
      "การเติมเงินล้มเหลว"
    when 2
      "คำสั่งขุดล้มเหลว"
    when 3
      "คำสั่งขุดเสร็จสมบูรณ์"
    end

  end

  def get_description
    case notification_type
    when 0
      "การเติมเงินเสร็จสมบูรณ์ ได้รับเครดิต #-#1 เครดิต".gsub("#-#1", data[:amount].to_s) + "\n" + get_time
    when 1
      "การเติมเงินล้มเหลว" + "\n" + get_time
    when 2
      "คำสั่งขุดล้มเหลว #-#1 / #-#2".gsub("#-#1", data[:finished_credit].to_s).gsub("#-#2", data[:credit].to_s) + "\n" + get_time
    when 3
      "คำสั่งขุดเสร็จสมบูรณ์ #-#1 / #-#2".gsub("#-#1", data[:finished_credit].to_s).gsub("#-#2", data[:credit].to_s) + "\n" + get_time
    end
  end

  def get_time
    Time.at(created_time).strftime('%D %r')
  end

  def get_url
    case notification_type
    when 0
      Rails.application.routes.url_helpers.transaction_history_index_path
    when 1
      Rails.application.routes.url_helpers.transaction_history_index_path
    when 2
      Rails.application.routes.url_helpers.order_history_index_path
    when 3
      Rails.application.routes.url_helpers.order_history_index_path
    end
  end

  def get_icon_background
    case notification_type
    when 0
      "#2B994E"
    when 1
      "#FF4F0F"
    when 2
      "#FF4F0F"
    when 3
      "#2B994E"
    end
  end

  def get_icon
    case notification_type
    when 0
      "money-bill-alt"
    when 1
      "money-bill-alt"
    when 2
      "exclamation"
    when 3
      "clipboard-check"
    end
  end

  def get_html
    "<a href='/notification/#{id}' class='notiShowup #{is_read ? '' : 'unread'}' id='#{id}' data-created_time='#{created_time}'><li><div class='notiIcon fa-fw'><span class='fa-layers fa-fw'><i class='fas fa-circle' style='color: #{get_icon_background}'></i><i class='fa-inverse fas fa-#{get_icon}' data-fa-transform='shrink-6'></i></span></div><div class='notiContent'><div class='notiContentHead'><span>#{get_title}</span></div><div class='notiContentTime'><span>#{get_time}</span></div></div></li></a>"
  end

  def send_cable
    ActionCable.server.broadcast 'noti_channel_'+user_id.to_s, data: get_html
  end

  def send_webpush
    user = User.find user_id
    if user.can_send_notification
      message = {
        title: get_raw_title,
        body: get_description,
        tag: "tag",
        icon: "https://s3-ap-southeast-1.amazonaws.com/playdigger/10560_956935148.jpg.png",
        url: get_url
      }
      begin
        Webpush.payload_send(
          message: JSON.generate(message),
          auth: user.auth,
          endpoint: user.endpoint_notification,
          p256dh: user.p256dh,
          ttl: 600,
          vapid: {
            public_key: Playdigger::Vapid['keypair'],
            private_key: Playdigger::Vapid['private_key']
          }
        )
      rescue
        user.can_send_notification = false
        user.save
      end
    end
  end

  private

  def update_created_time
    self.created_time = Time.now.to_i
  end
end
