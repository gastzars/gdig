class Announcement < ApplicationRecord
  before_create :update_created_time

  private

  def update_created_time
    self.created_time = Time.now.to_i
  end
end
