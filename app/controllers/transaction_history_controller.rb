class TransactionHistoryController < ApplicationController
  def index
    @total = MoneyTransaction.where(:user_id => current_user.id).count
    @total_page = (@total % 10) > 0 ? (@total / 10)+1 : (@total / 10)
    @page = (params['page'] || 1).to_i
  end
end
