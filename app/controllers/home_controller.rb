class HomeController < ApplicationController
  before_action :check_userlogin

  def index
    announce = Announcement.order(:created_time).last
    if announce && announce.created_time > current_user.last_seen_announcement_time
      @announce = announce
      current_user.last_seen_announcement_time = announce.created_time
      current_user.save
    end
  end

  def redirect_noti
    begin
      noti = Notification.where(id: params['id'], user_id: current_user.id).first
      if noti
        noti.is_read = true
        noti.save
        redirect_to noti.get_url
      else
        redirect_to home_path
      end
    rescue
      redirect_to home_path
    end
  end

  def update_notification_read_all
    data = {}
    success = false
    begin
      Notification.where(user_id: current_user.id, is_read: false).update_all({is_read: true})
      success = true
    rescue => e
      puts e
    end
    data['success'] = success
    render json: data.to_json
  end

  def update_notification_seen
    data = {}
    success = false
    begin
      if params['first_created_time']
        current_user.last_seen_notification_time = params['first_created_time'].to_i
        current_user.save
      end
      success = true
    rescue => e
      puts e
    end
    data['success'] = success
    render json: data.to_json
  end

  def load_notification
    data = {}
    success = false
    begin
      more_notification = Notification.where(:user_id => current_user.id).where("id < #{params[:last_id].to_s} AND created_time <= #{params[:last_created_time].to_s}").order(:created_time => :desc).limit(15)
      data_array = more_notification.collect{|noti| noti.get_html }
      data['data'] = data_array
      success = true
    rescue => e
      puts e
    end
    data['success'] = success
    render json: data.to_json
  end

  def update_user_notification
    data = {}
    success = false
    begin
      current_user.auth = params['auth']
      current_user.endpoint_notification = params['endpoint']
      current_user.p256dh = params['p256dh']
      current_user.can_send_notification = true
      current_user.save!
      success = true
    rescue => e
      puts e
    end
    data['success'] = success
    render json: data.to_json
  end

  def cancel_user_notification
    data = {}
    success = false
    begin
      if current_user.can_send_notification == true
        current_user.can_send_notification = false
        current_user.save!
      end
      success = true
    rescue => e
      puts e
    end
    data['success'] = success
    render json: data.to_json
  end

  def create_order
    begin
      validate_limit if params['islimit'] == 'true' || params['islimit'] == true
      validate_playserver_uri
      validate_username
      validate_order_count
      validate_credit
      validate_spam
      create_order_transaction
      @status = 'good'
      @redirect = true
      @message = 'คำสั่งขุดสำเร็จ'
    rescue => e
      @status = 'bad'
      @redirect = false
      @message = e.message
    end
  end

  def cancel_order
    begin
      order = OrderTransaction.where(:id => params["cancel_order_id"], user_id: current_user.id).first
      raise 'มีความผิดพลาดเกิดขึ้นกรุณาลองใหม่' if order.nil? || order.is_finished
      order_key = order.id.to_s+'#-#'+order.order_key
      digged = $redis.get(order_key).to_i
      solo_key = "solo:"+order.username+":"+order.url
      $redis.del(order_key)
      $redis.del(order_key+":credit")
      $redis.del(order_key+":to_dig")
      $redis.del(order_key+":lastupdate")
      $redis.del(order_key+":done")
      $redis.del(solo_key)
      $redis.lrem('playdigger:queue',0,order_key)
      order = OrderTransaction.find_by(:order_key => order_key.split("#-#")[1..-1].join("#-#"))
      order.finished_credit = digged
      order.is_finished = true
      order.status = 'D'
      order.save
      user = User.find(order.user_id)
      user.increment!(:credit, (order.credit - digged))
      @status = 'good'
      @message = 'ยกเลิกคำสั่งสำเร็จได้รับเครดิตคืน ' + (order.credit - digged).to_s
    rescue => e
      @status = 'bad'
      @message = e.message
    end
  end

  private

  def validate_spam
    if MoneyTransaction.where(:user_id => current_user.id).count == 0
      begin
        server = URI.decode(params['server'])
      rescue
        server = params['server']
      end
      if ($redis.get(params['username']) && $redis.get(params['username']).split("#-#")[0] == server && $redis.get(params['username']).split("#-#")[1].to_i != current_user.id) || OrderTransaction.where(url: server, username: params['username'].strip, is_free: true).sum(:finished_credit) > 200
        generated_key = generate_key(server, params['credit'].match(/\d+/).to_s.to_i, params['username'].strip)
        order = OrderTransaction.create({
          user_id: current_user.id,
          url: server,
          credit: params['credit'].match(/\d+/).to_s.to_i,
          order_key: generated_key,
          created_time: Time.now.to_i,
          username: params['username'].strip,
          is_finished: true,
          status: 'D',
          is_free: true
        })
        current_user.decrement!(:credit, params['credit'].match(/\d+/).to_s.to_i)
        @redirect = true
        raise "เราตรวจพบว่าคุณอาจจะเป็น Spam กรุณาติดต่อทีมงานเพื่อรับเครดิตคืน"
      else
        $redis.setex(params['username'], 3600*24*3, server+"#-#"+current_user.id.to_s)
      end
    end
  end

  def validate_order_count
    raise 'จำนวนคำสั่งเกินที่ระบบกำหนดไว้ กรุณารอให้คำสั่งที่ทำงานอยู่ทำเสร็จก่อน แล้วลองอีกครั้ง' if OrderTransaction.where(:user_id => current_user.id, :is_finished => false).count >= 2
  end

  def validate_limit
    raise 'จำนวนเครื่องสำหรับกำหนดการขุดต้องเป็นตัวเลข' unless params['ilimit'].match(/\d+/)
    raise 'จำนวนเครื่องสำหรับกำหนดการขุดต้องมากกว่า 0' if params['ilimit'].match(/\d+/).to_s.to_i == 0
    raise 'ผู้ใช้ฟรีไม่สามารถกำหนดความเร็วในการขุดได้ กรุณาเติมเงิน' if MoneyTransaction.where(:user_id => current_user.id).count == 0
  end

  def validate_playserver_uri
    raise 'Url ของ playserver ไม่ถูกรูปแบบ กรุณาลองใหม่' unless params['url'].to_s.include?("playserver.in.th/index.php/Vote/prokud") || params['url'].to_s.include?("playserver.in.th/index.php/Server")
    url = params['url'].to_s
    url.gsub!(/https?:\/\/playserver.in.th\/index.php\/Vote\/prokud\//,'')
    url.gsub!(/https?:\/\/playserver.in.th\/index.php\/Server\//,'')
    url.gsub!(/playserver.in.th\/index.php\/Vote\/prokud\//,'')
    url.gsub!(/playserver.in.th\/index.php\/Server\//,'')
    url.gsub!(/\//,'')
    begin
      server = url
      begin
        URI.parse(server)
      rescue URI::InvalidURIError
        server = URI.encode(server)
      end
      response = RestClient.post("http://playserver.co/index.php/Vote/ajax_getpic/#{server}", {})
      json_response = JSON.parse(response.to_s)
      raise 'Url ของ playserver ไม่ถูกรูปแบบ หรือระบบขุด playserver มีปัญหา กรุณาลองใหม่ภายหลัง' if json_response["success"] == false
      response = RestClient.get("http://playserver.in.th/index.php/Vote/prokud/#{server}")
      if response.to_s.include?("เซิฟเวอร์นี้ไม่ให้ใช้หลาย")
        if (params['islimit'] == 'true' || params['islimit'] == true) && params['ilimit'].match(/\d+/).to_s.to_i == 1
          begin
            this_server = URI.decode(url)
          rescue
            this_server = url
          end
          solo_key = "solo:"+params['username'].strip+":"+this_server
          if ($redis.get(solo_key))
            raise "Server นี้ไม่อนุญาติให้หลาย IP ขุดได้ และมีคำสั่งขุด ID นี้ที่ Server นี้อยู่แล้ว"
          else
            params['one_id'] = true
          end
        else
          raise 'Server นี้ไม่อนุญาติให้หลาย IP ขุดได้ กรุณาระบุความเร็วเป็น 1 เครื่องเท่านั้น'
        end
      end
    rescue JSON::ParserError
      raise 'มีความผิดพลาดเกิดขึ้น กรุณาลองใหม่ภายหลัง หรือติดต่อทีมงาน'
    rescue RuntimeError => e
      raise e.to_s
    rescue
      raise 'มีความผิดพลาดเกิดขึ้น กรุณาลองใหม่ภายหลัง หรือติดต่อทีมงาน'
    end
    params['server'] = url
  end

  def validate_username
    raise 'Username ต้องประกอบด้วยตัวอักษรภาษาอังกฤษหรือตัวเลขเท่านั้น' unless params['username'].match(/\A[a-zA-Z0-9_]{1,50}\z/)
  end

  def validate_credit
    raise 'จำนวนเครดิตต้องเป็นตัวเลข' unless params['credit'].match(/\d+/)
    raise 'จำนวนเครดิตไม่เพียงพอ' if (current_user.credit || 0) < params['credit'].match(/\d+/).to_s.to_i
    raise 'จำนวนเครดิตต้องมากกว่าหรือเท่ากับ 10' if params['credit'].match(/\d+/).to_s.to_i < 10
  end

  def create_order_transaction
    begin
      server = URI.decode(params['server'])
    rescue
      server = params['server']
    end
    generated_key = generate_key(server, params['credit'].match(/\d+/).to_s.to_i, params['username'].strip)
    is_free = MoneyTransaction.where(:user_id => current_user.id).count == 0
    order = OrderTransaction.create({
      user_id: current_user.id,
      url: server,
      credit: params['credit'].match(/\d+/).to_s.to_i,
      order_key: generated_key,
      created_time: Time.now.to_i,
      username: params['username'].strip,
      is_limit: (params['islimit'] == 'true' || params['islimit'] == true),
      instance_number: ((params['islimit'] == 'true' || params['islimit'] == true) ? params['ilimit'].match(/\d+/).to_s.to_i : nil),
      is_free: is_free
    })
    current_user.decrement!(:credit, params['credit'].match(/\d+/).to_s.to_i)
    assign_order_to_redis(order)
    instance_limit = params['ilimit'].match(/\d+/).to_s.to_i if (params['islimit'] == 'true' || params['islimit'] == true)
    is_free = MoneyTransaction.where(:user_id => current_user.id).count == 0
    CreateInstanceJob.perform_later(order.id.to_s+'#-#'+order.order_key, order.credit, instance_limit, is_free)
  end

  def generate_key(server, credit, username)
    begin
      URI.parse(server)
    rescue URI::InvalidURIError
      server = URI.encode(server)
    end
    o = [('a'..'z'), ('A'..'Z')].map(&:to_a).flatten
    return current_user.id.to_s+'#-#'+server+'#-#'+credit.to_s+'#-#'+username+'#-#'+(0...10).map{ o[rand(o.length)] }.join
  end

  def assign_order_to_redis(order)
    unless (params['islimit'] == 'true' || params['islimit'] == true)
      $redis.lpush('playdigger:queue', order.id.to_s+'#-#'+order.order_key)
    end
    if (params['one_id'] == true)
      $redis.setex("solo:"+order.username+":"+order.url, (order.credit*1.4*60).round, 1)
    end
    $redis.setex(order.id.to_s+'#-#'+order.order_key+':lastupdate', 3600*(24*3), Time.now.to_i)
    $redis.setex(order.id.to_s+'#-#'+order.order_key+':credit', 3600*(24*3), order.credit)
  end

  def check_userlogin
    unless current_user
      redirect_to '/'
    end
  end
end
