class RegController < ApplicationController
  before_action :check_userlogin, except: [:logout, :change_password]

  def index
  end

  def logout
    signout
    redirect_to '/'
  end

  def user_signin
    begin
      signin(params[:username], params[:password])
      @status = 'good'
      @message = 'การ Login สำเร็จ'
    rescue => e
      @status = 'bad'
      @message = e.message
    end
  end

  def change_password
    begin
      raise 'รหัสผ่านใหม่ไม่ตรงกัน' if params['new_password'] != params['new_password_confirmation']
      raise 'รหัสผ่านเก่าไม่ถูกต้อง' if current_user.password != params['old_password']
      raise 'รหัสผ่านใหม่และรหัสผ่านเก่าไม่สามารถเหมือนกันได้' if current_user.password == params['new_password']
      raise 'รหัสผ่านใหม่จะต้องมีความยาว 6-32 ตัวอักษร' unless params['new_password'].match(/^.{6,32}$/)
      current_user.password = params['new_password']
      current_user.save!
      @status = 'good'
      @message = 'การเปลี่ยนรหัสผ่านเสร็จสมบูรณ์'
    rescue => e
      @status = 'bad'
      @message = e.message
    end
  end

  def create
    begin
      change_params
      validate_recaptcha
      validate_params
      validate_account
      account = create_account
      login(account)
      @status = 'good'
      @message = 'การสมัครสมาชิกเสร็จสมบูรณ์'
    rescue => e
      @status = 'bad'
      @message = e.message
    end
  end

  private

  def change_params
    key = params['regk']
    params['regusername'] = params[$data_reg[key][:a]]
    params['regpassword'] = params[$data_reg[key][:b]]
    params['regpassword_confirm'] = params[$data_reg[key][:c]]
    params['regemail'] = params[$data_reg[key][:d]]
  end

  def check_userlogin
    if current_user
      redirect_to '/home'
    end
  end

  def validate_params
    raise 'Username จะต้องมีความยาว 6-32 ตัวอักษร และไม่มีตัวอักษรพิเศษ' unless params['regusername'].match(/^[\w]{6,32}$/)
    raise 'Password จะต้องมีความยาว 6-32 ตัวอักษร' unless params['regpassword'].match(/^.{6,32}$/)
    raise 'Password และ Confirm password ไม่ตรงกัน' if params['regpassword'] != params['regpassword_confirm']
    raise 'Format Email ไม่ถูกต้อง' unless params['regemail'].match(/^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)
  end

  def validate_account
    raise 'Username มีอยู่แล้วในระบบ' if User.find_by({ username: params['regusername'] })
    raise 'Email มีอยู่แล้วในระบบ' if User.find_by({ email: params['regemail'] })
  end

  def create_account
    user = User.create!(
      username: params['regusername'],
      password: params['regpassword'],
      email: params['regemail'],
      confirm_token: generate_token(params['regusername'])
    )
    UserConfirmMailer.registration_confirmation(params['regemail'], 'https://www.playdigger.com/verify_user?token='+user.confirm_token, params['regusername']).deliver_later
    return user
  end

  def generate_token(username)
    o = [('a'..'z'), ('A'..'Z')].map(&:to_a).flatten
    return username+'|'+(0...15).map{ o[rand(o.length)] }.join
  end
end
