class OrderHistoryController < ApplicationController
  before_action :check_userlogin, except: [:refresh, :update_order, :cancel_order, :report_instance]
  skip_before_action :verify_authenticity_token, only: [:update_order, :cancel_order, :report_instance]
 
  def index
    @total = OrderTransaction.where(:user_id => current_user.id).count
    @total_page = (@total % 10) > 0 ? (@total / 10)+1 : (@total / 10)
    @page = (params['page'] || 1).to_i
  end

  def report_instance
    CreateOneInstanceJob.perform_later(params['order_key'], (params['is_limit'] == true || params['is_limit'] == "True"), (params['is_free'] == true || params['is_free'] == "True"))
    render json: { success: true }.to_json
  end

  def update_order
    update_id = params['id'].to_i
    order = OrderTransaction.find(update_id)
    if (order.is_finished == false && order.status != 'C')
      ActionCable.server.broadcast 'order_update_channel_'+order.user_id.to_s, id: update_id
      talkable_order_key = update_id.to_s+"#-#"+order.order_key
      $redis.incr(talkable_order_key)
      $redis.set(talkable_order_key+':lastupdate', Time.now.to_i)
      if $redis.get(talkable_order_key).to_i >= order.credit
        solo_key = "solo:"+order.username+":"+order.url
        $redis.del(talkable_order_key)
        $redis.del(talkable_order_key+':credit')
        $redis.del(talkable_order_key+':to_dig')
        $redis.del(talkable_order_key+':done')
        $redis.del(talkable_order_key+':lastupdate')
        $redis.del(solo_key)
        $redis.lrem('playdigger:queue',0, talkable_order_key)
        order.status = "C"
        order.is_finished = true
        order.finished_credit = order.credit
        order.save
        user = User.find(order.user_id)
        noti = Notification.create!(
          user_id: user.id,
          notification_type: 3,
          data: {finished_credit: order.credit, credit: order.credit}
        )
        SendSystemNotificationJob.perform_later(noti.id)
      end
    end
    render json: { success: true }.to_json
  end

  def cancel_order
    update_id = params['id'].to_i
    order = OrderTransaction.find(update_id)
    if (order.is_finished == false && order.status != 'C')
      talkable_order_key = update_id.to_s+"#-#"+order.order_key
      solo_key = "solo:"+order.username+":"+order.url
      last_credit = $redis.get(talkable_order_key)
      $redis.del(talkable_order_key)
      $redis.del(talkable_order_key+':credit')
      $redis.del(talkable_order_key+':to_dig')
      $redis.del(talkable_order_key+':done')
      $redis.del(talkable_order_key+':lastupdate')
      $redis.del(solo_key)
      $redis.lrem('playdigger:queue',0, talkable_order_key)
      order.status = "D"
      order.finished_credit = last_credit.to_i
      order.is_finished = true
      order.save
      user = User.find(order.user_id)
      user.increment!(:credit, (order.credit - last_credit.to_i))
      noti = Notification.create!(
        user_id: user.id,
        notification_type: 2,
        data: {finished_credit: last_credit.to_i, credit: order.credit}
      )
      SendSystemNotificationJob.perform_later(noti.id)
    end
    render json: { success: true }.to_json
  end

  def refresh
    if params['id']
      if params['id'].class == String
        order_ids = params['id'].split(',')
      else
        order_ids = params['id']
      end
      orders = OrderTransaction.where(id: order_ids)
      data = orders.collect do |order|
        this_count = $redis.get(order.order_key).to_i
        {
          id: order.id,
          count: this_count,
          finished: order.is_finished,
          percent: (this_count/(order.credit/100.0)).round(0)
        }
      end
      render json: data.to_json
    else
      render json: [].to_json
    end
  end

  private

  def check_userlogin
    unless current_user
      redirect_to '/'
    end
  end
end
