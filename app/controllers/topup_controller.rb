class TopupController < ApplicationController
  before_action :check_userlogin, except: [:verify_true, :verify_paypal, :create_debit]
  skip_before_action :verify_authenticity_token, only: [:verify_paypal]
 
  AMOUNT_CASH = {
    '1' => 50,
    '2' => 90,
    '3' => 150,
    '4' => 300,
    '5' => 500,
    '6' => 1000
  }

  TRUE_MULTIPLIER = 20

  AMOUNT_CREDIT = {
    50 => (50*TRUE_MULTIPLIER),
    90 => (90*TRUE_MULTIPLIER),
    150 => (150*TRUE_MULTIPLIER),
    300 => (300*TRUE_MULTIPLIER),
    500 => (500*TRUE_MULTIPLIER),
    1000 => (1000*TRUE_MULTIPLIER)
  }

  PAYPAL_MULTIPLIER = 25
  DEBIT_MULTIPLIER = 25

  TRUE_PROMOTION = 1.07   # SET TO 1.0 to default
  PAYPAL_PROMOTION = 1.07 # SET TO 1.0 to default
  DEBIT_PROMOTION = 1.07  # SET TO 1.0 to default

  def index
  end

  def create_true
    begin
      validate_true_code
      create_true_transaction
      @status = 'good'
      @message = 'การเติมเงินสำเร็จ ระบบกำลังทำการตรวจสอบรหัสบัตรเติมเงิน'
    rescue => e
      @status = 'bad'
      @message = e.message
    end
  end

  def create_debit
    @status = 'bad'
    @message = ''
    begin
      raise "price can not be 0" if params['price'].to_i == 0
      if current_user && current_user.id == 1
        if params['id'].present?
          user = User.find_by(id: params['id'])
        elsif params['username'].present?
          user = User.find_by(username: params['username'])
        end
        if user
          if params['promotion'].present? && params['promotion'].to_i >= 0 && params['promotion'].to_i <= 100
            promotion = (params['promotion'].to_i/100.to_f)+1
          else
            promotion = DEBIT_PROMOTION
          end
          amount = (params['price'].to_i*DEBIT_MULTIPLIER*promotion).round(0)
          MoneyTransaction.create!(
            user_id: user.id,
            created_time: Time.now.to_i,
            status: 'B',
            topup_type: 'debit',
            amount: amount,
            price: params['price'].to_i
          )
          add_credit(user.id, amount, params['price'].to_i)
          SendNotificationJob.perform_later(user.id, user.username, params['price'].to_i, amount, 'โอนเงิน')
          @status = 'good'
          @message = 'การเติมเงินให้ลูกค้าเสร็จสมบูรณ์'
        else
          @message = 'Can not find user'
        end
      else
        @message = 'Only admin permitted'
      end
    rescue => e
      @status = 'bad'
      @message = e.message
    end
  end

  def verify_paypal
    if params['payment_status'] == 'Completed' && User.find_by(id: params['custom'].to_i)
      amount = (params['mc_gross'].to_i*PAYPAL_MULTIPLIER*PAYPAL_PROMOTION).round(0)
      MoneyTransaction.create!(
        user_id: params['custom'].to_i,
        created_time: Time.now.to_i,
        status: 'B',
        topup_type: 'paypal',
        amount: amount,
        price: params['mc_gross'].to_i
      )

      add_credit(params['custom'].to_i, amount, params['mc_gross'].to_i)
      user = User.find_by(id: params['custom'].to_i)
      if user
        SendNotificationJob.perform_later(user.id, user.username, params['mc_gross'].to_i, amount, 'บัตรเครดิต')
      end
    end
    render plain: 'SUCCEED'
  end

  def verify_true
    if request.remote_ip !~ /^103\.253\.74\./
      render plain: 'ERROR'
    else
      if params['status'] == '1' || params['status'] == 1
        item = MoneyTransaction.find_by(password: params['password'])
        if item && (item.status == 'A' || item.status == 'C')
          amount = (AMOUNT_CREDIT[AMOUNT_CASH[params['amount'].to_s].to_i]*TRUE_PROMOTION).round(0)
          item.update_attributes({
            status: 'B',
            price: AMOUNT_CASH[params['amount'].to_s],
            amount: amount
          })
          add_credit(item.user_id, amount, AMOUNT_CASH[params['amount'].to_s].to_i)
          user = User.find_by(id: item.user_id)
          if user
            SendNotificationJob.perform_later(user.id, user.username, AMOUNT_CASH[params['amount'].to_s], amount, 'True Money')
          end
        end
      else
        item = MoneyTransaction.find_by(password: params['password'])
        if item && item.status == 'A'
          item.update_attributes({status: 'C'})
          noti = Notification.create!(
            user_id: user_id,
            notification_type: 1,
            data: {}
          )
          SendSystemNotificationJob.perform_later(noti.id)
        end
      end
      render plain: 'SUCCEED'
    end
  end

  private

  def add_credit(user_id, amount, price)
    user = User.find(user_id)
    user.increment!(:credit, amount)
    user.increment!(:total_topup, price)
    noti = Notification.create!(
      user_id: user_id,
      notification_type: 0,
      data: {amount: amount}
    )
    SendSystemNotificationJob.perform_later(noti.id)
  end

  def create_true_transaction
    response = RestClient.get("https://www.true-topup.com/api.php?merchant=5917404TRU&password=#{params['cashno']}&resp_url=#{request.protocol}#{request.host_with_port}#{topup_true_verify_path}")
    if response.to_s == 'SUCCEED!' || response.to_s == 'SUCCEED' || response.to_s.strip == 'SUCCEED'
      unless MoneyTransaction.find_by(password: params['cashno'])
        MoneyTransaction.create!(
          user_id: current_user.id,
          password: params['cashno'],
          created_time: Time.now.to_i,
          status: 'A',
          topup_type: 'true'
        )
      else
        raise 'รหัสบัตรเติมเงินนี้มีอยู่แล้วในระบบ'
      end
    else
      raise 'มีความผิดพลาดเกิดขึ้น กรุณาลองใหม่'
    end
  end

  def validate_true_code
    raise 'รหัสบัตรเติมเงิน True Money ต้องมีความยาว 14 ตัวอักษร' if params['cashno'].length != 14
  end

  def check_userlogin
    unless current_user
      redirect_to '/'
    end
  end
end
