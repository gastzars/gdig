class AdministratorController < ApplicationController
  before_action :check_userlogin

  def index
  end

  def monitor
    @data = ($redis.lrange("playdigger:queue",0,100).collect{|g| [g, 'group'] }+OrderTransaction.where(is_finished: false, is_limit: true).collect{|t| [t.id.to_s+"#-#"+t.order_key, 'solo'] }).collect do |t,ty|
      {
        type: ty,
        key: t,
        current: $redis.get(t).to_i,
        credit: $redis.get(t+":credit").to_i,
        to_dig: $redis.get(t+":to_dig").to_i
      }
    end.sort_by{|h| h[:credit] - h[:current]} rescue []
  end

  private

  def check_userlogin
    unless current_user && current_user.id == 1
      redirect_to '/'
    end
  end
end
