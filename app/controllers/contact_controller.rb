class ContactController < ApplicationController
  before_action :check_userlogin
 
  def index
  end

  private

  def check_userlogin
    unless current_user
      redirect_to '/'
    end
  end
end
