class VerifyUserController < ApplicationController
  def index
    @success = false
    if params['token']
      user = User.find_by(confirm_token: params['token'])
      if user
        user.is_confirmed = true
        user.confirm_token = user.confirm_token+"used"
        user.save!
        @dup_ip = ($redis.get(request.remote_ip) != nil)
        @dup_ip = true if MailChecker(user.email) == false
        $redis.setex(request.remote_ip, 3600*24*3, 1)
        if @dup_ip == false
          user.increment!(:credit, BONUS_NEW_ACCOUNT)
          SendNotificationSignUpJob.perform_later(user.email, user.username)
        end
        @success = true
      end
    end
  end
end
