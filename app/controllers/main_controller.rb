class MainController < ApplicationController
  before_action :check_userlogin

  def index
  end

  private

  def check_userlogin
    if current_user
      redirect_to '/home'
    end
  end
end
