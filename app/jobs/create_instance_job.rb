require 'base64'
class CreateInstanceJob < ApplicationJob
  queue_as :default

  def perform(order_key, credit, instance_limit=nil, is_free=false)
    ec2 = Aws::EC2::Client.new(
      region: 'ap-southeast-1',
      access_key_id: 'AKIAJBTVH7KO6ZIIAEBA',
      secret_access_key: '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'
    )

    running_instances = $redis.llen('playdig:instances')
    idle_instances = $redis.llen('playdig:instances_idle')

    instance_to_run = (credit/100.0).round
    instance_to_run = 100 if instance_to_run > 100
    instance_to_run = 1 if instance_to_run < 1

    if instance_limit == nil
      need_to_run_instance = (instance_to_run - idle_instances)
      if (running_instances + need_to_run_instance) > 300
        need_to_run_instance = 300 - running_instances
      end
    else
      if instance_to_run > instance_limit
        need_to_run_instance = instance_limit
      else
        need_to_run_instance = instance_to_run
      end
      if (running_instances + need_to_run_instance) > 2000
        order = OrderTransaction.find_by(order_key: order_key.split('#-#')[1..-1].join('#-#'))
        order.status = 'D'
        order.is_finished = true
        order.finished_credit = 0
        order.save
        user = User.find(order.user_id)
        user.increment!(:credit, order.credit)
        return true
      end
    end

    if instance_limit == nil
      command = "#!/bin/bash\nsudo swapon /swapfile\n#{is_free ? "FREE_USER=True " : ""}python /home/ubuntu/gdig-worker/dig.py >> /home/ubuntu/out.log; sudo halt -P"
      instance_name = "group-worker"
    else
      command = "#!/bin/bash\nsudo swapon /swapfile\nFOCUS=#{order_key} IS_I_LIMIT=true #{is_free ? "FREE_USER=True " : ""}python /home/ubuntu/gdig-worker/dig.py >> /home/ubuntu/out.log; sudo halt -P"
      instance_name = "solo-"+order_key.split("#-#")[0].to_s
    end

    ec2.run_instances({
      image_id: "ami-4c560430",
      instance_type: "t2.nano",
      key_name: 'playdigger',
      max_count: need_to_run_instance,
      min_count: 1,
      user_data: Base64.encode64(command),
      security_group_ids: ["sg-22dcf245"],
      instance_initiated_shutdown_behavior: "terminate",
      tag_specifications: [
        {
          resource_type: "instance",
          tags: [
            {
              key: "Name",
              value: instance_name,
            },
          ]
        }
      ]
    })
    order = OrderTransaction.find_by(order_key: order_key.split('#-#')[1..-1].join('#-#'))
    order.status = 'B'
    order.save
  end
end

