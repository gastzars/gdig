class CreateOneInstanceJob < ApplicationJob
  queue_as :default

  def perform(order_key=nil, is_limit=nil, is_free=false)
    ec2 = Aws::EC2::Client.new(
      region: 'ap-southeast-1',
      access_key_id: 'AKIAJBTVH7KO6ZIIAEBA',
      secret_access_key: '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'
    )

    if is_limit != nil && order_key != nil
      command = "#!/bin/bash\nsudo swapon /swapfile\nFOCUS=#{order_key} IS_I_LIMIT=true #{is_free ? "FREE_USER=True " : ""}python /home/ubuntu/gdig-worker/dig.py >> /home/ubuntu/out.log; sudo halt -P"
      instance_name = "solo-"+order_key.split("#-#")[0].to_s
    else
      command = "#!/bin/bash\nsudo swapon /swapfile\n#{is_free ? "FREE_USER=True " : ""}python /home/ubuntu/gdig-worker/dig.py >> /home/ubuntu/out.log; sudo halt -P"
      instance_name = "group-worker"
    end

    ec2.run_instances({
      image_id: "ami-4c560430",
      instance_type: "t2.nano",
      key_name: 'playdigger',
      max_count: 1,
      min_count: 1,
      user_data: Base64.encode64(command),
      security_group_ids: ["sg-22dcf245"],
      instance_initiated_shutdown_behavior: "terminate",
      tag_specifications: [
        {
          resource_type: "instance",
          tags: [
            {
              key: "Name",
              value: instance_name,
            },
          ]
        }
      ]
    })
  end
end
