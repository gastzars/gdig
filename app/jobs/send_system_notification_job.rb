class SendSystemNotificationJob < ApplicationJob
  queue_as :default

  def perform(id)
    noti = Notification.find id
    noti.send_cable
    noti.send_webpush
  end
end
