class SendNotificationSignUpJob < ApplicationJob
  queue_as :default

  def perform(user_email, username)
    access_token = "EAAQQc0ZCEDZAcBAGuds1ZCvFIcHGffpZBnSeLZCkLX5NO9q10ClsEjRk3cIXjNjwZAAB5MOUDqUesJA8TPhBfGN6BHPRZCWiUc2C0ZCYbuVZAMir0vguieU8ZAB9PyFqiieAsHr5yyZAWZBHPF1vEkw4WtawVrUlIr0jy6tFMqBigtBWpQZDZD"
    to_send = ['t_100001257018717', 't_100001153331468']
    graph = Koala::Facebook::API.new(access_token)
    to_send.each do |send_id|
      message = "มีคนสมัครใช้บริการด้วย\nEmail : #{user_email}\nUsername: #{username}"
      graph.put_connections(send_id, 'messages', {message: message})
    end
  end
end
