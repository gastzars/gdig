class NotiChannel < ApplicationCable::Channel
  def subscribed
    stream_from "noti_channel_"+params[:userId].to_s
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
