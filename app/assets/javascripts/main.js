// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

function regSubmit(token) {
  var valuesToSubmit = $('form#reg_form').serialize();
  $('#regusername').prop('disabled', true);
  $('#regpassword').prop('disabled', true);
  $('#regpassword_confirm').prop('disabled', true);
  $('#regemail_').prop('disabled', true);
  $.ajax({
    type: "POST",
    url: $('form#reg_form').attr('action'),
    data: valuesToSubmit,
  }).error(function(e){
    $('#regusername').prop('disabled', false);
    $('#regpassword').prop('disabled', false);
    $('#regpassword_confirm').prop('disabled', false);
    $('#regemail_').prop('disabled', false);
  }).success(function(e){
    $('#regusername').prop('disabled', false);
    $('#regpassword').prop('disabled', false);
    $('#regpassword_confirm').prop('disabled', false);
    $('#regemail_').prop('disabled', false);
  });
}
