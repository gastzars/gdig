// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
//= require cable_noti

function topupChange() {
  $('#text_tobehide').css('display', 'none')
  $('#true_money_container').css('display', 'none')
  $('#paypal_container').css('display', 'none')
  $('#debit_container').css('display', 'none')
  var x = document.getElementById("pm_method").value;
  switch(x) {
    case 'เลือก':
      $('#text_tobehide').css('display', 'block');
      break;
    case '(1 บาท / 20 เครดิต) บัตร True Money':
      $('#true_money_container').css('display', 'block');
      break;
    case '(1 บาท / 25 เครดิต) Visa/MasterCard ผ่าน Paypal':
      $('#paypal_container').css('display', 'block');
      break;
    case '(1 บาท / 25 เครดิต) โอนเงิน':
      $('#debit_container').css('display', 'block');
      break;
  }
}

var setEvent = false
var loadDone = false
var loading = false
var lastUpdateNoti = 0

$(document).on('click', function(e) {
    if ( $(e.target).closest('#toggleNoti').length ) {
        $('#notiList').toggle()
        if ($(".notiShowup").length > 0 && (Math.floor(Date.now() / 1000) - lastUpdateNoti) > 2) {
          first_element = $($(".notiShowup")[0])
          first_created_time = first_element.data('created_time')
          $.ajax({
            url: "/update_notification_seen",
            method: "POST",
            data: {first_created_time: first_created_time},
            dataType: "json",
          })
          lastUpdateNoti = Math.floor(Date.now() / 1000)
          $("#noti_counter")[0].innerHTML = 0
          $("#noti_counter").css('display', 'none')
        }
        if (setEvent == false) {
          setEvent = true
          $("#notiListData").on("scroll", function() {
            if($("#notiListData").scrollTop() >= $("#notiListData").height() - 50 && loadDone == false && loading == false) {
              loading = true
              last_element = $($(".notiShowup")[$(".notiShowup").length-1])
              last_id = last_element[0].id
              last_created_time = last_element.data('created_time')
              $.ajax({
                url: "/load_notification",
                method: "POST",
                data: {last_id: last_id, last_created_time: last_created_time},
                dataType: "json",
              }).done(function( msg ) {
                if (msg.success == true) {
                  var i = msg.data.length
                  for(var j =0; j < i; j++) {
                    addOldNoti(msg.data[j])
                  }
                  if (i < 15) {
                    loadDone = true
                    $("#loadNoti").remove()
                  }
                }
                loading = false
              });
            }
          })
        }
    }else if ( ! $(e.target).closest('#notiList').length ) {
        $('#notiList').hide();
    }
});
