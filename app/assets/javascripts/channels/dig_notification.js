App.dig_notification = App.cable.subscriptions.create({channel: "DigNotificationChannel", userId: userId}, {
  connected: function() {
  },

  disconnected: function() {
  },

  received: function(data) {
    updateData(data.id)
  }
});
