// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery
//= require jquery_ujs
//= require fontawesome-all.min

var messageSnackQueue = [];
var currentShow = false;
function alertSnack(message, messageStatus) {
  messageSnackQueue.push([message, messageStatus]);
  displayQueue();
}

function displayQueue() {
  if (currentShow == false && messageSnackQueue.length > 0) {
    var thisMessage = messageSnackQueue.pop();
    var message = thisMessage[0];
    var messageStatus = thisMessage[1];
    currentShow = true;
    $('#snackbar').removeClass('good').removeClass('bad').removeClass('normal').addClass(messageStatus);
    $('#snackbar span')[0].innerHTML = message;
    $('#snackcontainer').css('bottom', '0px');
    setTimeout(function(){
      $('#snackcontainer').css('bottom', '-50px');
      setTimeout(function(){
        currentShow = false;
        displayQueue();
      }, 500);
    }, 3500);
  }
}

function prepApp() {
  if (typeof initQueue !== 'undefined') {
    for(var i=0; i< initQueue.length; i++) {
      messageSnackQueue.push(initQueue.pop());
    }
    displayQueue();
  }
}
$(document).ready(prepApp);

function addNewNoti(data) {
  $("#notiListData ul").prepend(data)
  $("#noti_counter")[0].innerHTML = parseInt($("#noti_counter")[0].innerHTML)+1
  if ($("#noti_counter").css('display') == 'none') {
    $("#noti_counter").css('display', 'block')
  }
  $('#noNoti').remove()
}

function addOldNoti(data) {
  $("#notiListData ul").append(data)
}

function readall() {
  $(".unread").removeClass('unread')
  $.ajax({
    url: "/update_notification_read_all",
    method: "POST",
    data: {},
    dataType: "json",
  })
}
