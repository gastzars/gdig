// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

//= require cable_both

dig_data = {}
new_dig_data = {}

function updateDigDataNew() {
  var dup = new_dig_data
  new_dig_data = {}
  for (var key in dup) {
    if (dup.hasOwnProperty(key)) {
      var row = $("tr[data-order_id="+key+"]")
      var curdig = row.data('current_dig')
      var new_curdig = curdig + dup[key]
      row.data('current_dig', new_curdig)
      row.children('.ord_max').children('.cur_ord')[0].innerHTML = new_curdig
    }
  }
}

function updateData(id) {
  if (new_dig_data[id] === undefined) {
    new_dig_data[id] = 1
  } else {
    new_dig_data[id] = new_dig_data[id] + 1
  }
}

$( document ).ready(function() {
  var digs = $('.dig_row')
  var dig_count = digs.length;
  for(var i = 0; i < dig_count; i++) {
    var _this = $(digs[i]);
    var this_id = _this.data('order_id')
    var this_dig = _this.data('current_dig')
    dig_data[this_id] = this_dig
  }
  setInterval(function() { updateDigDataNew() }, 1000);
});

var setEvent = false
var loadDone = false
var loading = false
var lastUpdateNoti = 0

$(document).on('click', function(e) {
    if ( $(e.target).closest('#toggleNoti').length ) {
        $('#notiList').toggle()
        if ($(".notiShowup").length > 0 && (Math.floor(Date.now() / 1000) - lastUpdateNoti) > 2) {
          first_element = $($(".notiShowup")[0])
          first_created_time = first_element.data('created_time')
          $.ajax({
            url: "/update_notification_seen",
            method: "POST",
            data: {first_created_time: first_created_time},
            dataType: "json",
          })
          lastUpdateNoti = Math.floor(Date.now() / 1000)
          $("#noti_counter")[0].innerHTML = 0
          $("#noti_counter").css('display', 'none')
        }
        if (setEvent == false) {
          setEvent = true
          $("#notiListData").on("scroll", function() {
            if($("#notiListData").scrollTop() >= $("#notiListData").height() - 50 && loadDone == false && loading == false) {
              loading = true
              last_element = $($(".notiShowup")[$(".notiShowup").length-1])
              last_id = last_element[0].id
              last_created_time = last_element.data('created_time')
              $.ajax({
                url: "/load_notification",
                method: "POST",
                data: {last_id: last_id, last_created_time: last_created_time},
                dataType: "json",
              }).done(function( msg ) {
                if (msg.success == true) {
                  var i = msg.data.length
                  for(var j =0; j < i; j++) {
                    addOldNoti(msg.data[j])
                  }
                  if (i < 15) {
                    loadDone = true
                    $("#loadNoti").remove()
                  }
                }
                loading = false
              });
            }
          })
        }
    }else if ( ! $(e.target).closest('#notiList').length ) {
        $('#notiList').hide();
    }
});
