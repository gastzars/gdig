// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

//= require cable_both

dig_data = {}
new_dig_data = {}

function showAdvanced() {
    var chboxs = document.getElementsByName("islimit");
    var vis = "none";
    for(var i=0;i<chboxs.length;i++) { 
        if(chboxs[i].checked){
         vis = "block";
            break;
        }
    }
    document.getElementById("advanced_content").style.display = vis;
    document.getElementById("i_showing").style.display = vis;
}

function updateDigDataNew() {
  var dup = new_dig_data
  new_dig_data = {}
  for (var key in dup) {
    if (dup.hasOwnProperty(key)) {
      var pgb = $(".progress_bar[data-order_id="+key+"]")
      var curdig = pgb.data('current_dig')
      var dig_max = pgb.data('dig_max')
      var new_curdig = curdig + dup[key]
      pgb.data('current_dig', new_curdig)
      var pg = pgb.children('.progress')
      var percent = (new_curdig/(dig_max/100)).toFixed(0)
      pg.css('width', percent+"%")
      var row = $("tr[data-order_id="+key+"]")
      row.children('.ord_max').children('.cur_ord')[0].innerHTML = new_curdig
    }
  }
}

function cancelConfirm() {
  if ($("#fader").css('display') == "block") {
    $("#fader").fadeToggle(100)
  }
}

function pleaseConfirm() {
  if ($("#fader").css('display') == "none") {
    var target_url = $("#url").val()
    if (target_url == "" || target_url === undefined) {
      alertSnack('Server Url ไม่สามารถเว้นว่างไว้ได้', 'bad');
      return
    }
    if ($("#credit").val() == "" || $("#credit").val() === undefined) {
      alertSnack('จำนวนเครดิตไม่สามารถเว้นว่างไว้ได้', 'bad');
      return
    }
    if (isNaN(parseInt($("#credit").val())) == false && parseInt($("#credit").val()) < 10) {
      alertSnack('จำนวนเครดิตต้องมากกว่าหรือเท่ากับ 10', 'bad');
      return
    }
    if (target_url.includes("http")) {
      var splitted = target_url.split("/")
      var split_count = splitted.length
      target_url = splitted[split_count-1]
    }
    $("#confirm_server_name")[0].innerHTML = decodeURIComponent(target_url)
    $("#confirm_credit")[0].innerHTML = $("#credit").val()
    $("#confirm_id")[0].innerHTML = $("#username").val()
    $("#confirm_instance")[0].innerHTML = $("#ilimit").val()
    $("#fader").fadeToggle(100)
  }
}

function cancelOrder(order_target_id) {
  if ($("#cancelOrderFader").css('display') == "none") {
    $("#cancel_order_id").val(order_target_id)
    $("#cancelOrderFader").fadeToggle(100)
  }
}

function cancelCancel() {  
  if ($("#cancelOrderFader").css('display') == "block") {
    $("#cancelOrderFader").fadeToggle(100)
  }
}

function hideAnnounce() {
  if ($("#announceFader").css('display') == "block") {
    $("#announceFader").fadeToggle(100)
  }
}

function updateData(id) {
  if (new_dig_data[id] === undefined) {
    new_dig_data[id] = 1
  } else {
    new_dig_data[id] = new_dig_data[id] + 1
  }
}

$( document ).ready(function() {
  var digs = $('.progress_bar')
  var dig_count = digs.length;
  for(var i = 0; i < dig_count; i++) {
    var _this = $(digs[i]);
    var this_id = _this.data('order_id')
    var this_dig = _this.data('current_dig')
    dig_data[this_id] = this_dig
  }
  setInterval(function() { updateDigDataNew() }, 1000);
});


var swRegistration
var initialized = false

if ('serviceWorker' in navigator && 'PushManager' in window) {
  navigator.serviceWorker.register('/serviceworker.js')
  .then(function(swReg) {
    swRegistration = swReg;
    swRegistration.pushManager.getSubscription().then(function(subscription) {
      isSubscribed = !(subscription === null);
      updateSubscriptionOnServer(subscription);
    });
  }).catch(function(error) {
  });
} else {
}

navigator.serviceWorker.ready.then((serviceWorkerRegistration) => {
  if (initialized == false) {
    serviceWorkerRegistration.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: window.vapidPublicKey
    }).then(function(s) {
      var data = {
        endpoint: s.endpoint,
        p256dh: btoa(String.fromCharCode.apply(null, new Uint8Array(s.getKey('p256dh')))).replace(/\+/g, '-').replace(/\//g, '_'),
        auth: btoa(String.fromCharCode.apply(null, new Uint8Array(s.getKey('auth')))).replace(/\+/g, '-').replace(/\//g, '_')
      }
    })
  }
});

if (!("Notification" in window)) {
} else if (Notification.permission !== 'denied') {
  Notification.requestPermission(function (permission) {
    if (permission === "granted") {
      if (initialized == false) {
        swRegistration.pushManager.subscribe({
          userVisibleOnly: true,
          applicationServerKey: window.vapidPublicKey
        }).then(function(s) {
          updateSubscriptionOnServer(s)
        })
      }
    }
  });
}

function updateSubscriptionOnServer(s) {
  if (s && currentSubscribed == false) {
    currentSubscribed = true
    var data = {
      endpoint: s.endpoint,
      p256dh: btoa(String.fromCharCode.apply(null, new Uint8Array(s.getKey('p256dh')))).replace(/\+/g, '-').replace(/\//g, '_'),
      auth: btoa(String.fromCharCode.apply(null, new Uint8Array(s.getKey('auth')))).replace(/\+/g, '-').replace(/\//g, '_')
    }
    $.ajax({
      url: "/update_user_notification",
      method: "POST",
      data: {endpoint: data.endpoint, p256dh: data.p256dh, auth: data.auth},
      dataType: "json",
    })
  } else if (s === null && currentSubscribed == true) {
    currentSubscribed = false
    $.ajax({
      url: "/cancel_user_notification",
      method: "POST",
      data: {},
      dataType: "json",
    })
  }
}

var setEvent = false
var loadDone = false
var loading = false
var lastUpdateNoti = 0

$(document).on('click', function(e) {
    if ( $(e.target).closest('#toggleNoti').length ) {
        $('#notiList').toggle()
        if ($(".notiShowup").length > 0 && (Math.floor(Date.now() / 1000) - lastUpdateNoti) > 2) {
          first_element = $($(".notiShowup")[0])
          first_created_time = first_element.data('created_time')
          $.ajax({
            url: "/update_notification_seen",
            method: "POST",
            data: {first_created_time: first_created_time},
            dataType: "json",
          })
          lastUpdateNoti = Math.floor(Date.now() / 1000)
          $("#noti_counter")[0].innerHTML = 0
          $("#noti_counter").css('display', 'none')
        }
        if (setEvent == false) {
          setEvent = true
          $("#notiListData").on("scroll", function() {
            if($("#notiListData").scrollTop() >= $("#notiListData").height() - 50 && loadDone == false && loading == false) {
              loading = true
              last_element = $($(".notiShowup")[$(".notiShowup").length-1])
              last_id = last_element[0].id
              last_created_time = last_element.data('created_time')
              $.ajax({
                url: "/load_notification",
                method: "POST",
                data: {last_id: last_id, last_created_time: last_created_time},
                dataType: "json",
              }).done(function( msg ) {
                if (msg.success == true) {
                  var i = msg.data.length
                  for(var j =0; j < i; j++) {
                    addOldNoti(msg.data[j])
                  }
                  if (i < 15) {
                    loadDone = true
                    $("#loadNoti").remove()
                  }
                }
                loading = false
              });
            }
          })
        }
    }else if ( ! $(e.target).closest('#notiList').length ) {
        $('#notiList').hide();
    }
});

