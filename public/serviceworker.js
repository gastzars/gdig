self.addEventListener("push", (event) => {
  data = JSON.parse(event.data.text())
  let title = data.title;
  let body = data.body;
  let tag = data.tag;
  let icon = data.icon;

  event.waitUntil(
    self.registration.showNotification(title, { body: body, icon: icon, tag: tag, data: { url: data.url } })
  )
});

self.addEventListener('notificationclick', function(event) {
  console.log('[Service Worker] Notification click Received.');
  console.log(event)
  event.notification.close();

  event.waitUntil(
    clients.openWindow(event.notification.data.url)
  );
});
